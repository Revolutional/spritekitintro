//
//  GameScene.swift
//  SpriteKitIntro
//
//  Created by LDC on 12/7/15.
//  Copyright (c) 2015 LDC. All rights reserved.
//

import SpriteKit

class GameScene: SKScene
{
    var cannon:SKSpriteNode!
    var touchLocation:CGPoint = CGPointZero
    
    override func didMoveToView(view: SKView)
    {
        /* Setup your scene here */
        /* let myLabel = SKLabelNode(fontNamed:"Chalkduster")
        myLabel.text = "Hello, World!";
        myLabel.fontSize = 45;
        myLabel.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame));
        
        self.addChild(myLabel)
        */
        
        
        cannon = self.childNodeWithName("//cannon") as! SKSpriteNode
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
       /* Called when a touch begins */
        
        /*
        for touch in touches {
            let location = touch.locationInNode(self)
            
            let sprite = SKSpriteNode(imageNamed:"Spaceship")
            
            sprite.xScale = 0.5
            sprite.yScale = 0.5
            sprite.position = location
            
            let action = SKAction.rotateByAngle(CGFloat(M_PI), duration:1)
            
            sprite.runAction(SKAction.repeatActionForever(action))
            
            self.addChild(sprite)
        }
        */
        touchLocation = touches.first!.locationInNode(self);
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?)
    {
        touchLocation = touches.first!.locationInNode(self);

    }
   
    override func update(currentTime: CFTimeInterval)
    {
        /* Called before each frame is rendered */
        let percent = touchLocation.x / size.width
        let newAngle = percent * 180 - 180
        cannon.zRotation = CGFloat(newAngle) * CGFloat(M_PI) / 180.0
    }
}
